package org.mamontov.note;
 
public class Spisok<T> {
    private NodeList<T> mNext;
    private NodeList<T> mFirst;

    public void add(T value) {
    	NodeList<T> newElement = new NodeList<T>();
		newElement.setValue(value);
			if (mNext == null) {
	        	mFirst = newElement;
	            mNext = newElement;
	        } else {
	        	mNext.setNext(newElement);
	            mNext = newElement;
	            
	        }
    }
    
    // ��������� ������ � ���������
    private NodeList<T> reversRecursion(NodeList<T> node) {
    	 if (node == null) 
             return null; 
         if (node.getNext() == null) { 
             return node; 
         } 
         NodeList<T> node1 = reversRecursion(node.getNext()); 
         node.getNext().setNext(node); 
         node.setNext(null); 
         return node1; 
    }
    
    public void reversRecursion() {
    	mFirst = this.reversRecursion(mFirst);
    }
    
    // ��������� ������ ��� ��������
    public void revers(){
    	NodeList<T> first = mFirst;
    	NodeList<T> second = mFirst.getNext();
    	NodeList<T> third = second.getNext();
     
    	first.setNext(null);
    	second.setNext(first);
     
    	NodeList<T> current = third;
    	NodeList<T> previous = second;
     
    	while (current != null) {
    		NodeList<T> next = current.getNext();
    	    current.setNext(previous);
     
    	    previous = current;
    	    current = next;
    	}
     
    	mFirst = previous;
    }


	void printList() {
		NodeList<T> link = mFirst;
        while (link != null) {
            System.out.print(" " + link.getValue());
            link = link.getNext();
        }
    }
}