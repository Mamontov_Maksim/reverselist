package org.mamontov.note;

public class NodeList<T> {
    private NodeList<T> mNext;
    private T mValue;
    
	public NodeList<T> getNext() {
		return mNext;
	}
	
	public void setNext(NodeList<T> mNext) {
		this.mNext = mNext;
	}
	
	public T getValue() {
		return mValue;
	}
	
	public void setValue(T mValue) {
		this.mValue = mValue;
	}
}
