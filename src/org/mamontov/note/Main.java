package org.mamontov.note;

public class Main {
	
	
	public static void main(String[] args) {
		
		System.out.println("With out recursion");
		Spisok<Integer> myList = new Spisok<>();
        myList.add(1);
        myList.add(2);
        myList.add(3);
        myList.add(4);
        myList.add(100);
        System.out.print("Befor flip : ");
        myList.printList();
        myList.revers();
        System.out.print("\nAfter flip : ");
        myList.printList();
        
        System.out.println("\nWith on recursion");
        Spisok<String> mylistString = new Spisok<>();
        mylistString.add("13");
        mylistString.add("11");
        mylistString.add("7");
        mylistString.add("1");
        System.out.print("Befor flip : ");
        mylistString.printList();
        mylistString.reversRecursion();
        System.out.print("\nAfter flip : ");
        mylistString.printList();
       
    }
}
